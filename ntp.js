// https://support.google.com/chrome/answer/157179?hl=en
// TODO: Switch storage.sync to storage.local?

function debounce(func, threshold) {
  var timeout;
  return function debounced() {
    var obj = this, args = arguments;
    function delayed() {
      func.apply(obj, args);
      timeout = null;
    };
    if (timeout) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(delayed, threshold);
  };
}

function dataURItoBlob(dataURI) {
  var mime = dataURI.split(',')[0].split(':')[1].split(';')[0];
  var binary = atob(dataURI.split(',')[1]);
  var array = [];
  for (var i = 0; i < binary.length; i++) {
     array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], {type: mime});
}

function handleFile(file) {
  var url = URL.createObjectURL(file);
  document.body.style.backgroundImage = `url(${url})`;

  var reader = new FileReader();

  reader.addEventListener("load", function() {
    // console.log(reader.result);
    // debugger;
    // preview.src = reader.result;
    chrome.storage.local.set({
      backgroundimage: reader.result,
    }, function(items) {
    });
  }, false);
  reader.readAsDataURL(file);
}

chrome.storage.local.get({
  backgroundimage: null,
}, function(items) {
  if (!items.backgroundimage) return;
  document.body.style.backgroundImage = `url(${items.backgroundimage})`;
  // var url = URL.createObjectURL(dataURItoBlob(items.backgroundimage));
  // document.body.style.backgroundImage = `url(${url})`;
});

document.addEventListener("DOMContentLoaded", function() {
  chrome.runtime.getPlatformInfo(function(info) {
    var span = document.getElementById("bookmarksbar-shortcut");
    if (info.os == "mac") {
      span.innerText = "⌘ + Shift + B";
    }
    else {
      // Windows and Linux have the same shortcuts
      span.innerText = "Ctrl + Shift + B";
    }
  });

  var links = document.getElementsByTagName("a");
  for (var i=0; i < links.length; i++) {
    var link = links[i];
    if (link.href.substr(0,9) == "chrome://") {
      link.addEventListener("click", function() {
        chrome.tabs.create({ url: this.href });
      });
    }
  }

  var toggle = document.getElementById("toggle-settings");
  toggle.addEventListener("click", function() {
    var div = document.getElementById("settings");
    div.style.display = "block";
    this.style.display = "none";

    // Tells the browser that we *can* drop on this target
    // We only attach these after the settings are opened
    function cancel(e) {
      e.preventDefault();
    }
    window.addEventListener("dragover", cancel);
    window.addEventListener("dragenter", cancel);
    window.addEventListener("drop", function(e) {
      e.preventDefault();
      var dt = e.dataTransfer;
      handleFile(dt.files[0]);
    });
  });

  var title = document.getElementById("title");
  title.addEventListener("input", function() {
    document.title = this.value;
    chrome.storage.sync.set({
      title: this.value,
    });
  });

  var backgroundcolor = document.getElementById("backgroundcolor");
  var backgroundcolorhex = document.getElementById("backgroundcolorhex");

  backgroundcolor.addEventListener("input", function() {
    backgroundcolorhex.value = this.value;
    document.body.style.backgroundColor = this.value;
  });
  backgroundcolor.addEventListener("input", debounce(function() {
    chrome.storage.sync.set({
      backgroundcolor: this.value,
    });
  }, 100));

  backgroundcolorhex.addEventListener("input", function() {
    backgroundcolor.value = this.value;
    document.body.style.backgroundColor = this.value;
  });
  backgroundcolorhex.addEventListener("input", debounce(function() {
    // We debounce this event, because if we spam storage.set too much, we will get this error:
    // Unchecked runtime.lastError while running storage.set: This request exceeds the MAX_WRITE_OPERATIONS_PER_MINUTE quota.
    chrome.storage.sync.set({
      backgroundcolor: this.value,
    });
  }, 100));

  var reset = document.getElementById("reset");
  reset.addEventListener("click", function() {
    chrome.storage.sync.clear();
    chrome.storage.local.clear();
    var value = "#ffffff";
    backgroundcolorhex.value = value;
    backgroundcolor.value = value;
    document.body.style.backgroundImage = '';
    document.body.style.backgroundColor = value;
  });

  var browse = document.getElementById("browse");
  var browse2 = document.getElementById("browse2");
  browse.addEventListener("click", function() {
    browse2.click();
  });
  browse2.addEventListener("change", function() {
    handleFile(this.files[0]);
  });

  chrome.storage.sync.get({
    title: "New Tab",
    backgroundcolor: "#ffffff",
  }, function(items) {
    document.title = items.title;
    title.value = items.title;
    var value = items.backgroundcolor;
    backgroundcolorhex.value = value;
    backgroundcolor.value = value;
    document.body.style.backgroundColor = value;
  });
});
